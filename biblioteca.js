let almacenamiento = window.localStorage;

let dialogoNuevo, dialogoEditar;
let nuevaCelda, usuario, registro, indiceEditar;
let lista = [];

// Clase Usuario
//====================================================
class Usuario {
  _id = "";
  _nombre = "";
  _username = "";
  _email = "";
  _telefono = "";

  setId(id) {
    this._id = id;
  }
  getId() {
    return this._id;
  }
  setNombre(nombre) {
    this._nombre = nombre;
  }
  getNombre() {
    return this._nombre;
  }
  setUsername(username) {
    this._username = username;
  }
  getUsername() {
    return this._username;
  }
  setEmail(email) {
    this._email = email;
  }
  getEmail() {
    return this._email;
  }
  setTelefono(telefono) {
    this._telefono = telefono;
  }
  getTelefono() {
    return this._telefono;
  }
  toString() {
    return this._id + " " + this._nombre + " " + this._username + " " + this._email + " " + this._telefono;
  }
}

// Inicio de ejecucion
//====================================================
function iniciar() {

  // Dialogo para crear nuevo usuario
  //--------------------------------------------
  dialogoNuevo = $("#dialogoNuevo").dialog({
    title: "Crear nuevo usuario",
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    resizable: false,
    buttons: [
      {
        text: "Guardar nuevo",
        class: "btn btn-success",
        id: "btnGuardar",
        click: agregarNuevo
      },
      {
        text: "Cancelar",
        class: "btn btn-secondary",
        id: "btnCancelarNuevo",
        click: function () {
          $(this).dialog("close");
        }
      }
    ]
  });

  // Dialogo para editar usuario
  //--------------------------------------------
  dialogoEditar = $("#dialogoEditar").dialog({
    title: "Editar usuario",
    autoOpen: false,
    height: 400,
    width: 350,
    modal: true,
    resizable: false,
    buttons: [
      {
        text: "Actualizar",
        class: "btn btn-success",
        id: "btnActualizar",
        click: actualizarUsuario
      },
      {
        text: "Cancelar",
        class: "btn btn-secondary",
        id: "btnCancelarEditar",
        click: function () {
          $(this).dialog("close");
        }
      }
    ]
  });

  // Evento click para boton Agregar nuevo usuario
  //------------------------------------------------
  $("#btnAgregar").click(function () {

    let nuevoId = obtenerNuevoId();
    $("input[name = 'id']").val(nuevoId);

    dialogoNuevo.dialog("open");
  });

  // Evento click para boton Cancelar en formulario nuevo usuario
  //---------------------------------------------------------------
  $("#btnCancelarNuevo").click(function () {
    dialogoNuevo.dialog("close");
  });

  // Evento click para boton Cancelar en formulario editar usuario
  //---------------------------------------------------------------
  $("#btnCancelarEditar").click(function () {
    dialogoEditar.dialog("close");
  });

  // Evento click para boton Editar un usuario
  //--------------------------------------------
  $(document).on('click', '.btn-editar', function () {
    registro = $(this).closest('tr');

    llenarFormEditar(registro);
    dialogoEditar.dialog("open");
  });

  // Evento click para boton Eliminar un usuario
  //--------------------------------------------
  $(document).on('click', '.btn-eliminar', function () {

    registro = $(this).closest('tr');
    var id = parseInt($(registro).find('td:eq(0)').text());
    actualizarEliminacion(id);
    $(this).closest('tr').remove();

  });

  obtenerDatosIniciales();
  llenarTabla();
}

// Obtener datos de URL de API
//====================================================
async function obtenerDatosIniciales() {
  const result = await fetch("https://jsonplaceholder.typicode.com/users");
  const res = await result.json();
  almacenamiento.setItem("users", JSON.stringify(res));
}

// Llenar tabla al inicio de la ejecucion
//====================================================
function llenarTabla() {
  lista = JSON.parse(almacenamiento.getItem("users"));

  lista.forEach(element => {
    usuario = new Usuario();
    usuario.setId(element.id);
    usuario.setNombre(element.name);
    usuario.setUsername(element.username);
    usuario.setEmail(element.email);
    usuario.setTelefono(element.phone);

    agregarFilaTabla(usuario);
  });
}


// Agregar nuevo usuario al almacenamiento local
//====================================================
function almacenarNuevo(usuario) {
  lista = JSON.parse(almacenamiento.getItem("users"));

  lista.push({
    id: parseInt(usuario.getId()),
    name: usuario.getNombre(),
    username: usuario.getUsername(),
    email: usuario.getEmail(),
    phone: usuario.getTelefono()
  });

  almacenamiento.setItem("users", JSON.stringify(lista));
}


// Actualizar datos de usuario en el almacenamiento local
//=======================================================
function actualizarAlmacenamiento(usuario) {
  if (lista) {
    indiceEditar = lista.findIndex(element => element.id === parseInt(usuario.getId()));

    usuario = {
      id: parseInt(usuario.getId()),
      name: usuario.getNombre(),
      username: usuario.getUsername(),
      email: usuario.getEmail(),
      phone: usuario.getTelefono()
    };

    if(indiceEditar) {
      lista.fill(usuario, indiceEditar, indiceEditar+1);
    }

    almacenamiento.setItem("users", JSON.stringify(lista));
  }
}

// Eliminar registro de usuario en el almacenamiento local
//========================================================
function actualizarEliminacion(id) {
  if (lista) {
    indiceEditar = lista.findIndex(element => element.id === id);

    if(indiceEditar) {
      lista.splice(indiceEditar, 1);
    }

    almacenamiento.setItem("users", JSON.stringify(lista));
  }
}

// Evento click para boton Actualizar en formulario Editar usuario
//================================================================
function actualizarUsuario() {
  if (registro) {
    usuario = new Usuario();
    usuario.setId($("input[name = 'idEditar']").val());
    usuario.setNombre($("input[name = 'nombreEditar']").val());
    usuario.setUsername($("input[name = 'unameEditar']").val());
    usuario.setEmail($("input[name = 'emailEditar']").val());
    usuario.setTelefono($("input[name = 'telEditar']").val());

    $(registro).find('td:eq(1)').text(usuario.getNombre());
    $(registro).find('td:eq(2)').text(usuario.getUsername());
    $(registro).find('td:eq(3)').text(usuario.getEmail());
    $(registro).find('td:eq(4)').text(usuario.getTelefono());
    registro = null;
  }

  actualizarAlmacenamiento(usuario);

  $("#formEditar")[0].reset();
  $("#dialogoEditar").dialog("close");
}

// Evento click para boton Guardar nuevo en formulario Nuevo usuario
//================================================================
function agregarNuevo() {

  usuario = new Usuario();
  usuario.setId($("input[name = 'id']").val());
  usuario.setNombre($("input[name = 'nombre']").val());
  usuario.setUsername($("input[name = 'uname']").val());
  usuario.setEmail($("input[name = 'email']").val());
  usuario.setTelefono($("input[name = 'tel']").val());

  almacenarNuevo(usuario);
  agregarFilaTabla(usuario);

  $("#formNuevo")[0].reset();
  $("#dialogoNuevo").dialog("close");
}

// Agrega una fila al final de la tabla con los datos ingresados
//================================================================
function agregarFilaTabla(usuario) {
  //let dataTable = $("#datatable")[0];
  let dataTable = $("#tbody")[0];
  let nuevaLinea = dataTable.insertRow(-1);

  let nuevaCelda = nuevaLinea.insertCell(0);
  nuevaCelda.textContent = usuario.getId();
  nuevaCelda = nuevaLinea.insertCell(1);
  nuevaCelda.textContent = usuario.getNombre();
  nuevaCelda = nuevaLinea.insertCell(2);
  nuevaCelda.textContent = usuario.getUsername();
  nuevaCelda = nuevaLinea.insertCell(3);
  nuevaCelda.textContent = usuario.getEmail();
  nuevaCelda = nuevaLinea.insertCell(4);
  nuevaCelda.textContent = usuario.getTelefono();
  nuevaCelda = nuevaLinea.insertCell(5);
  nuevaCelda.innerHTML = '<button type="button" class="btn btn-success btn-editar">&nbsp;&nbsp;&nbsp;Editar&nbsp;&nbsp;&nbsp;</button>';
  nuevaCelda = nuevaLinea.insertCell(6);
  nuevaCelda.innerHTML = '<button type="button" class="btn btn-danger btn-eliminar">Eliminar</button>';
}

// Obtiene automaticamente el nuevo Id al agregar un usuario
//================================================================
function obtenerNuevoId() {

  let filas = $("#tbody").find('tr').length;
  let listaFilas = document.getElementsByTagName("tr");
  let ultimoId = parseInt(listaFilas[filas].cells[0].innerHTML);

  return ultimoId + 1;
}

// Rellena los campos del formulario Editar usuario con los
// datos de la fila seleccionada
//================================================================
function llenarFormEditar(registro) {
  var _id = $(registro).find('td:eq(0)').text();
  var _nombre = $(registro).find('td:eq(1)').text();
  var _username = $(registro).find('td:eq(2)').text();
  var _email = $(registro).find('td:eq(3)').text();
  var _tel = $(registro).find('td:eq(4)').text();

  $("input[name = 'idEditar']").val(_id);
  $("input[name = 'nombreEditar']").val(_nombre);
  $("input[name = 'unameEditar']").val(_username);
  $("input[name = 'emailEditar']").val(_email);
  $("input[name = 'telEditar']").val(_tel);
}